package de.awacademy.blogproject.article;

import de.awacademy.blogproject.user.User;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.Instant;

public class ArticleDTO {
    /**
     * Zur Erstellung eines Articles benötigt.
     */
    //Felder

    @NotEmpty
    @Size(max = 25, message = "Titel darf max 25 Zeichen enthalten")
    private String title;
    @NotEmpty
    private String text;
    private Instant date;
    private User user;


    //Konstruktor
    public ArticleDTO() {
    }

    public ArticleDTO(@NotEmpty @Pattern(regexp = "^[a-zA-Z0-9_-]+$", message = "Bitte nur Buchstaben, Zahlen, Bindestrich und Unterstrich verwenden") String title, @NotEmpty String text) {
        this.title = title;
        this.text = text;
    }


    //Getter & Setter

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
