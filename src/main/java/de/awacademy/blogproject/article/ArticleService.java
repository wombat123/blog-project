package de.awacademy.blogproject.article;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleService {

    //Felder
    private ArticleRepository articleRepository;

    //Konstruktor

    public ArticleService() {
    }
    @Autowired
    public ArticleService(@Qualifier("articles") ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    //Methoden

    public void addArticle(Article article) {
        articleRepository.save(article);
    }

    public Article findArticleById(long id) {
        return articleRepository.findById(id);
    }

    public void deleteArticle(Article article) {
        articleRepository.delete(article);
    }

    public void save(Article article) {
        articleRepository.save(article);
    }

    public void updateArticle(Article article, long id) {
        Article currentArticle = articleRepository.getOne(id);
        currentArticle.setTitle(article.getTitle());
        currentArticle.setText(article.getText());
        articleRepository.save(currentArticle);
    }

    public Article getOne(long id) {
        return articleRepository.getOne(id);
    }
    //Getter

    public List<Article> getArticleRepository() {
        return articleRepository.findAllByOrderByPostedAtDesc();
    }
}
