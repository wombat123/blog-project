package de.awacademy.blogproject.article;

import de.awacademy.blogproject.comment.Comment;
import de.awacademy.blogproject.user.User;
import javax.persistence.*;
import java.time.Instant;
import java.util.List;

/**
 * Artikel können vom Admin geschrieben werden. Sie haben einen Autor (user) und ihnen können Kommentare (many to one) beigefügt werden.
 */
@Entity
public class Article {

    //Felder

    @Id
    @GeneratedValue
    private long id;

    private String title;
    private String text;
    private Instant postedAt;

    @ManyToOne
    private User user;
    @OneToMany(mappedBy = "article")
    private List<Comment> comments;

    //Konstruktor
    public Article() {
    }

    public Article(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public Article(String title, String text, Instant postedAt, User user) {
        this.title = title;
        this.user = user;
        this.text = text;
        this.postedAt = postedAt;
    }


    //Getter und Setter

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public List<Comment> getComments() {
        return comments;
    }
}
