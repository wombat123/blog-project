package de.awacademy.blogproject.session;

import de.awacademy.blogproject.user.User;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;
import java.util.UUID;

@Entity
public class Session {

    //Felder
    @Id
    private String id = UUID.randomUUID().toString();
    @ManyToOne
    private User user;
    private Instant expiresAt;

    //Konstruktoren

    public Session() {
    }

    public Session(User user, Instant expiresAt) {
        this.user = user;
        this.expiresAt = expiresAt;
    }


    //Getter und Setter
    public String getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setExpiresAt(Instant expiresAt) {
        this.expiresAt = expiresAt;
    }
}
