package de.awacademy.blogproject.session;

import javax.validation.constraints.NotEmpty;

public class LoginDTO {

    //Felder
    @NotEmpty
    private String username;
    @NotEmpty
    private String password;

    //Konstruktor

    public LoginDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }


    //Getter

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
