package de.awacademy.blogproject.session;

import de.awacademy.blogproject.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.Instant;

@Controller
public class SessionController {

    //Felder
    private SessionRepository sessionRepository;
    private UserRepository userRepository;

    //Konstruktor
    @Autowired
    public SessionController(SessionRepository sessionRepository, UserRepository userRepository) {
        this.sessionRepository = sessionRepository;
        this.userRepository = userRepository;
    }


    //Methoden

    //>>>>>>>>>LOGIN<<<<<<<<<

    /**
     * Get Mapping für den Login.
     * Dem Model wird ein neues LoginDTO übergeben. Dies kann der User im PostMapping befüllen.
     * @param model  verknüpft das Java-Object mit dem login-Template
     * @return      das login-Template auf welchem sich das Login-Formular befindet
     */
    @GetMapping("/login")
    public String login(Model model) {

        model.addAttribute("login", new LoginDTO("", ""));
        return "login";
    }

    /**
     * Post Mapping für den Login.
     * Prüft ob die eingegebenen Daten mit einer Datenzeile in der Datenbank verbunden sind.
     * @param login Vom user eingegebenen Daten zum Ableich in der DB.
     * @param bindingResult Fehlerprüfung (Felder dürfen nicht bleiben)
     * @param response  Bei erfolgreichem Login wird ein Cookie mit der SessionId erstellt.
     * @return          redirectet zur Home-Seite(Beitragsübersicht)
     */
    @PostMapping("/login")
    public String login(@Valid @ModelAttribute("login") LoginDTO login, BindingResult bindingResult, HttpServletResponse response) {

        if (!(userRepository.existsByUsernameAndPassword(login.getUsername(), login.getPassword()))) {
            bindingResult.addError(new FieldError("login", "password", "Keine Übereinstimmung"));
            return "login";
        }

        Session session = new Session(userRepository.findByUsernameAndPassword(login.getUsername(), login.getPassword()), Instant.now().plusSeconds(7 * 24 * 60 * 60));
        sessionRepository.save(session);

        Cookie cookie = new Cookie("sessionId", session.getId());
        response.addCookie(cookie);

        return "redirect:/home";
    }


}

