package de.awacademy.blogproject.comment;

import de.awacademy.blogproject.user.User;

import javax.validation.constraints.NotEmpty;
import java.time.Instant;

public class CommentDTO {

    //Felder

    @NotEmpty
    private String text;
    private Instant date;
    private User user;

    //Konstruktor

    //Methoden

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
