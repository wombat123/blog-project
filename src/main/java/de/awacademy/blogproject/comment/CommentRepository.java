package de.awacademy.blogproject.comment;

import de.awacademy.blogproject.article.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {

    Comment findById(long id);

    Optional<List<Comment>> findAllByArticle(Article article);
}

