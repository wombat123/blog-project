package de.awacademy.blogproject.comment;

import de.awacademy.blogproject.article.Article;
import de.awacademy.blogproject.user.User;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

/**
 * comment-Klasse
 * Kommentare werden am Ende des Artikels angezeigt. Sie sind jeweils einem User und einem Artikel zugeordnet.
 */
@Entity
public class Comment {

    @Id
    @GeneratedValue
    private long Id;

    //Felder
    private String text;
    private Instant date;

    @ManyToOne
    private User user;

    @ManyToOne
    private Article article;

    //Konstruktor

    public Comment() {
    }

    public Comment(String text, Instant date, User user, Article article) {
        this.text = text;
        this.date = date;
        this.user = user;
        this.article = article;
    }

    //Methoden

    //Getter& Setter

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Article getArticle() {
        return article;
    }

    public long getId() {
        return Id;
    }
}
