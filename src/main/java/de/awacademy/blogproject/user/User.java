package de.awacademy.blogproject.user;

import de.awacademy.blogproject.article.Article;
import de.awacademy.blogproject.comment.Comment;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class User {

    //Primärschlüssel
    @Id
    @GeneratedValue
    private long value;

    //Felder
    private String username;
    private String password;
    private boolean admin;

    @OneToMany(mappedBy = "user")
    private List<Comment> userComments;

    @OneToMany(mappedBy = "user")
    private List<Article> userArticles;

    //Konstruktor

    public User() {
    }

    public User(String username, String password){
    this.username=username;
    this.password=password;
}
    //Methoden


    //Getter&Setter

    public long getValue() {
        return value;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

/*    public List<Comment> getUserComments() {
        return userComments;
    }

    public void setUserComments(List<Comment> userComments) {
        this.userComments = userComments;
    }*/
}
