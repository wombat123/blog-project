package de.awacademy.blogproject.user;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RegisterDTO {

    //Felder
    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z0-9_-]+$", message = "Bitte nur Buchstaben, Zahlen, Bindestrich und Unterstrich verwenden")
    private String username;
    @Size(min = 7, message = "Passwort zu kurz")
    private String password1;
    private String password2;


    //Konstruktor

    public RegisterDTO(String username, String password1, String password2) {
        this.username = username;
        this.password1 = password1;
        this.password2 = password2;
    }


    //Getter
    public String getUsername() {
        return username;
    }

    public String getPassword1() {
        return password1;
    }

    public String getPassword2() {
        return password2;
    }
}
