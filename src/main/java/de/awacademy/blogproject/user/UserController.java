package de.awacademy.blogproject.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UserController {

    //Felder
    private UserRepository userRepository;

    //Konstruktor
    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    //Methoden
    //>>>>>>>>>REGISTER<<<<<<<<<
    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("registration", new RegisterDTO("", "", ""));

        return "register";
    }

    @PostMapping("/register")
    public String register(@Valid @ModelAttribute("registration") RegisterDTO register, BindingResult bindingResult) {

        if (!register.getPassword1().equals(register.getPassword2())) {
            bindingResult.addError(new FieldError("registration", "password2", "Keine Übereinstimmung"));
        }
        if (userRepository.existsByUsername(register.getUsername())) {
            bindingResult.addError(new FieldError("registration", "username", "Schon vergeben"));
        }
        if (bindingResult.hasErrors()) {
            return "register";
        }
        User user = new User(register.getUsername(), register.getPassword1());
        userRepository.save(user);

        return "redirect:/login";
    }
    //>>>>>>>> MAKE REGISTERED USER TO ADMIN<<<<<<<<

    /**
     * Zeigt dem Admin die Liste aller registrierten User an. Doer ist zu sehen, wer bereits admin ist
     * der Rest kann vom Admin zum Admin "befördert" werden.
     * @param model übergibt die List an das userlist-Template, um alle User anzuzeigen.
     * @return          userlist-Template, mit der Liste aller User
     */
    @GetMapping("/userlist")
    public String userlist(Model model) {
        model.addAttribute("userList", userRepository.findAll());
        return "userlist";
    }

    /**
     * Gibt dem Admin die Möglichkeit einen registrierten User den Admin-Status zuzuweisen.
     * @param value expliziete User-ID für korrekte Zuweisung
     * @param sessionUser muss den Admin-Status haben
     * @return            die User-Liste
     */

    @PostMapping("/userlist/makeAdmin/{value}")
    public String makeAdmin(@PathVariable long value, @ModelAttribute("sessionUser") User sessionUser) {
        User user = userRepository.findById(value);
        user.setAdmin(true);
        userRepository.save(user);

        return "redirect:/userlist";
    }

}
