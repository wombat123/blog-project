package de.awacademy.blogproject;

import de.awacademy.blogproject.article.Article;
import de.awacademy.blogproject.article.ArticleDTO;
import de.awacademy.blogproject.article.ArticleService;
import de.awacademy.blogproject.comment.Comment;
import de.awacademy.blogproject.comment.CommentDTO;
import de.awacademy.blogproject.comment.CommentRepository;
import de.awacademy.blogproject.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.validation.Valid;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Controller
public class MessageController {

    private ArticleService articleService;
    private CommentRepository commentRepository;


    //Konstruktor

    @Autowired
    public MessageController(ArticleService articleService, CommentRepository commentRepository) {
        this.commentRepository=commentRepository;
        this.articleService = articleService;
    }

    //Methoden

    //>>>>>>>>VIEW ARTICLE<<<<<<<<<

    /**
     * Startseite mit allen bereits geschriebenen Beiträgen.
     * anonyme User können die Einträge nur sehen.
     * Admins können neue Beiträge verfasseb
     * @param model wird die Liste aller Einträge übergeben
     * @return   das Home-Template
     */
    @GetMapping("/home")
    public String home(Model model){
        model.addAttribute("articles", articleService.getArticleRepository());
        return "home";
    }

    /**
     * Zeigt den jeweiligen Artikel an.
     * Anonymer User kann den Artikel & die Kommentare lesen
     * Registrierter User kann außerdem Kommentare verfassen (& eigene Kommentare Löschen)
     * Admin kann außerdem Artikel/Kommentare Löschen.
     * @param model Wird der Artikel, die Kommentare(falls vohanden) sowie ein neues KommentarDTO
     *              übergeben
     * @param id    expliziete Artikel ID
     * @param sessionUser anhand des sessionUsers wird geprüft, welche FUnktionen sichtbar sind
     * @return   entweder die SIcht für eingeloggte oder anonyme User
     */
    @GetMapping("/article/{id}")
    public String viewArticle(Model model, @PathVariable long id,@ModelAttribute("sessionUser") User sessionUser){
        Article article = articleService.findArticleById(id);
        model.addAttribute("article", article);
        List<Comment> comments = article.getComments();
        model.addAttribute("kommentare",comments);

        Optional<User> user = Optional.ofNullable(sessionUser);
        if(user.isPresent()) {
            CommentDTO comment = new CommentDTO();
            model.addAttribute("kommentar", comment);
            return "articleloggedin";
        }
        return "article";
    }
    //>>>>>>>>WRITE COMMENT AS REGISTERED USER<<<<<<<<<<<<

    /**
     * als reg. User kann man einen Kommentar schreiben
     * @param commentDTO wird mit den eingegeben Daten des users befüllt
     * @param sessionUser muss vorhanden sein, sonnst kann kein Kommentar geschrieben werden
     * @param bindingResult prüft ob alle Felder (Text) befüllt wurden
     * @param id expliziete Artikel ID ordnet dem Kommentar einen Artikel zu
     * @return    redirectet zur Artikelseite
     */
    @PostMapping("/article/{id}/kommentar")
    public String kommentarSchreiben( @Valid @ModelAttribute("kommentar") CommentDTO commentDTO,
                                      @ModelAttribute("sessionUser") User sessionUser, BindingResult bindingResult,
                                     @PathVariable long id){
        if (bindingResult.hasErrors()) {
            return "/article/{id}";
        }
        Article article = articleService.getOne(id);
        Comment comment = new Comment(commentDTO.getText(), Instant.now(),sessionUser, article);

        commentRepository.save(comment);

        return "redirect:/article/{id}";

    }
    //>>>>>>>>WRITE NEW ARTICLE AS ADMIN<<<<<<<<<<<<<<

    /**
     * Admin kann einen neuen Artikel verfassen
     * @param model übergiebt dem Model ein neues ArticleDTO
     * @return  das writearticle-Template
     */
    @GetMapping("/home/newarticle")
    public String beitragSchreiben(Model model){

        ArticleDTO articleDTO = new ArticleDTO("","");

        model.addAttribute("article",articleDTO);

        return "writearticle";
    }

    /**
     * Der Admin befüllt die Felder des articleDTO, um einen neuen Artikel zu verfassen
     * @param articleDTO beinhaltet die Eingaben des Admins
     * @param bindingResult prüft, ob alle Felder befüllt wurden
     * @param sessionUser muss Admin sein, um diese Funktion nutzen zu dürfen
     * @return  die Home-Seite (Artikelübersicht)
     */
    @PostMapping("/home/newarticle/write")
    public String beitragSchreiben(  @Valid @ModelAttribute("article") ArticleDTO articleDTO,BindingResult bindingResult,
                                       @ModelAttribute("sessionUser") User sessionUser ){
        if (bindingResult.hasErrors()) {


            return "writearticle";
        }

        Article article = new Article(articleDTO.getText(), articleDTO.getTitle(), Instant.now(),sessionUser);

        articleService.save(article);

        return "redirect:/home";

    }
    //>>>>>>>>>>>>>>>>>>AS REGISTERED USER/ ADMIN DELETE YOUR/ANY COMMENT<<<<<<<<<<<<<<<<<<<<

    /**
     * reg. User kann seine eigenen Kommentare löschen
     * Admin kann jeden Kommentar löschen
     *
     * @param id des zugehörigen Artikels
     * @param kommentarID Id des zu löschenden Kommentars
     * @param sessionUser je nach user-art wird die Funktion angezeigt
     * @return die Artikelübersicht
     */
    @PostMapping("/article/{id}/{kommentarID}/delete")
    public String deleteMyKommentar(@PathVariable long id, @PathVariable long kommentarID,
                                    @ModelAttribute("sessionUser") User sessionUser){
        Comment comment = commentRepository.findById(kommentarID);
        commentRepository.delete(comment);

        return "redirect:/article/{id}";
    }
    //>>>>>>>>>>>>>>>EDIT ARTICLE AS ADMIN<<<<<<<<<<<<<<<<<<<<

    /**
     * Admin kann alle Artikel editieren.
     * @param model wird der zu editierende Artikel übergebn
     * @param id an Hand der explizieten Artikel-ID kann der richtige Artikel benutzt werden
     * @return das editarticle-Template mit den Werten des Artikels
     */
    @GetMapping("/article/{id}/edit")
    public String editArticle(Model model,@PathVariable long id){

        Article article = articleService.findArticleById(id);
        model.addAttribute("article",article);

        return "editarticle";
    }

    /**
     * Lässt den Admin veränderungen in den Feldern Text & Title eines Artikels machen
     * @param model bei fehlern wird die edit-Seite, ohne Änderungen,erneut aufgerufen mitsammt der Daten des Artikle-Objektes
     * @param article das zu ändernde Objekt
     * @param theBindingResult prüft die Validierung (kein Feld darf leer bleiben)
     * @param id                expliziete Artikle ID
     * @param redirectAttributes sorg dafür, dass kein neues Objekt erstellt wird, sondern das bestehende verändert wird
     * @return                  die Artikel-Seite
     */
    @PostMapping("/article/{id}/edit")
    public String updateArticle(Model model, @Valid @ModelAttribute Article article,
                                BindingResult theBindingResult, @PathVariable long id,
                                RedirectAttributes redirectAttributes) {

        Article currentArticle = articleService.getOne(id);
        long articleId = currentArticle.getId();
        redirectAttributes.addAttribute("articleId", articleId);

        if (theBindingResult.hasErrors()){
            model.addAttribute("article", currentArticle);
            return "writearticle";
        }
        articleService.updateArticle(article, id);

        return "redirect:/home";
    }

    //>>>>>>>>>>>> AS ADMIN DELETE ARTICLE INCLUDING ALL COMMENTS<<<<<<<<<<<<<<

    /**
     * Admin kann Artikel mit dazugehörigen Kommentaren löschen
     * @param id expliziete Artikel ID
     * @param sessionUser muss admin sein
     * @return  die Artikelübersicht
     */
    @PostMapping("/article/{id}/deleteall")
    public String deleteArticle(@PathVariable long id, @ModelAttribute("sessionUser") User sessionUser){
        Article temp =articleService.findArticleById(id);

        Optional<List<Comment>> articleKommentars = commentRepository.findAllByArticle(temp);
        if(articleKommentars.isPresent()) {
            for (Comment k : articleKommentars.get()){
                commentRepository.delete(k);
            }
        }
        articleService.deleteArticle(temp);

        return "redirect:/home";
    }
}